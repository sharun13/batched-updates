# **Batched Updates In React.js**

React is a high performance front-end JavaScript framework developed by Facebook that we use to build composable user interfaces predictably and efficiently using declarative code. React gives better performance than other JavaScript languages due to it’s implementation of a virtual DOM and **`Batched Updates`** is one way through which the performance gain is achieved. 

In class components, we have several ways to construct and alter the component’s state, for e.g. we change the state by calling **`setState`** method. These changes cause parts of the component to re-render, and possibly its children as well. Instead of one by one, React does updates in batches, reducing the number of component renders. This is called as **`Batched Updates`**. 

Without `batched updates`, updates were batched only inside react event handlers i.e Updates inside of `promises`, `setTimeout`, native event handlers, or any other events were not batched in React by default. For small applications, this re-rendering process may not impact significantly, but as the application grows, the number of nested components will increase. Therefore, if a parent component executes an un-batched state update, the entire component tree will be re-rendered per state update. To avoid this, react introduced the `batched updates` functionality. Let's consider an example given in the official react documentation. 

```
// When only React events are batched.
setTimeout(() => {
  setCount(count => count + 1);
  setFlag(flag => !flag);
  // React will render twice, once for each state update (no batching)
}, 1000);

// In batched updates : updates inside of timeouts, promises,
// native event handlers or any other event are batched.
setTimeout(() => {
  setCount(count => count + 1);
  setFlag(flag => !flag);
  // React will only re-render once at the end (that's batching!)
}, 1000);
```

React batches multiple `setState()` calls into a single update for performance. Batch updating is an interesting feature, that combines state updates and improves performance considerably.

The main idea is that no matter how many `setState` calls are made inside a React event handler or synchronous lifecycle method, it will be batched into a single update. That is only one single re-render will eventually happen and its purpose is to prevent unnecessary rendering. 

### **References**
- [Reactjs.org](https://reactjs.org/blog/2022/03/08/react-18-upgrade-guide.html)
- [Bits and Pieces](https://blog.bitsrc.io/automatic-batching-in-react-18-what-you-should-know-d50141dc096e#:~:text=React%20uses%20batching%20to%20group,support%20batching%20for%20browser%20events.)
- [Medium](https://medium.com/swlh/react-state-batch-update-b1b61bd28cd2)
- [Stack Overflow](https://stackoverflow.com/questions/63979492/what-does-batching-update-mean-in-react)